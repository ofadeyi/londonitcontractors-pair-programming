package com.itcg.pairing;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by ofadeyi on 26/04/16.
 */
public class RomanNumeral {

    private Map<Integer, String> convertionTable = new HashMap<>();

    public RomanNumeral() {
        convertionTable.put(1, "I");
        convertionTable.put(4, "IV");
        convertionTable.put(5, "V");
        convertionTable.put(9, "IX");
        convertionTable.put(10, "X");
        convertionTable.put(40, "XL");
        convertionTable.put(50, "L");
        convertionTable.put(90, "XC");
        convertionTable.put(100, "C");
        convertionTable.put(400, "CD");
        convertionTable.put(500, "D");
        convertionTable.put(900, "CM");
        convertionTable.put(1000, "M");
    }

    public boolean canConvert(int number) {
        if (number > 0)
            return true;
        return false;
    }


    public String convert(int number) {
        if (!canConvert(number)) {
            throw new RuntimeException("Could not find number");
        }
        String roman  = "";
        int remainder = number;

        do{
            int highestDecimal = findHighestDecimal(remainder);
            remainder = remainder - highestDecimal;
            roman += convertionTable.get(highestDecimal);
        }while(remainder > 0);

        return roman;
    }

    public int findHighestDecimal(int number) {
        Optional<Integer> highestDecimal = convertionTable
                .keySet()
                .stream()
                .sorted()
                .filter(x -> x <= number)
                .max(Comparator.naturalOrder());

        return highestDecimal.orElseThrow(() -> new RuntimeException("Could not find number"));
    }

    public int lengthNumber(int number) {
        return String.valueOf(number).length();
    }
}
