package com.itcg.pairing

import spock.lang.Specification

/**
 * Created by ofadeyi on 26/04/16.
 */
class RomanNumeralSpec extends Specification {

    def RomanNumeral romanNumeral

    def setup() {
        romanNumeral = new RomanNumeral()
    }

    def cleanup() {
        romanNumeral = null
    }

    def "should return false for negative numbers"(number, convertable) {
        expect:
        convertable == romanNumeral.canConvert(number)

        where:
        number | convertable
        -42    | false
        0      | false
        1      | true
        42     | true
    }

    def "should convert 1, 2, 3"(decimal, roman) {
        expect:
        roman == romanNumeral.convert(decimal)

        where:
        decimal | roman
        1       | 'I'
        2       | 'II'
        3       | 'III'
    }

    def "should return the length of a number as string"(number, length) {
        expect:
        length == romanNumeral.lengthNumber(number)

        where:
        number | length
        1      | 1
        47     | 2
        1990   | 4
    }


    def "should return the highest power of ten"(inputNumber, highestDecimal) {
        expect:
        highestDecimal == romanNumeral.findHighestDecimal(inputNumber)

        where:
        inputNumber | highestDecimal
        1           | 1
        10          | 10
        100         | 100
        1000        | 1000
        2000        | 1000
    }

    def "should return the highest decimal"(inputNumber, highestDecimal) {
        expect:
        highestDecimal == romanNumeral.findHighestDecimal(inputNumber)

        where:
        inputNumber | highestDecimal
        47          | 40
        99          | 90
        445         | 400
        545         | 500
        745         | 500
        945         | 900
    }

    def "should return an exception when converting a negative number"(){
        when:
        romanNumeral.convert(-42)

        then:
        thrown(RuntimeException)
    }

    def "should return an exception when converting 0"(){
        when:
        romanNumeral.convert(0)

        then:
        RuntimeException ex = thrown()
        ex.message == 'Could not find number'
    }

    def "should return the roman representation"(number, roman) {
        expect:
        roman == romanNumeral.convert(number)

        where:
        number | roman
        4      | 'IV'
        8      | 'VIII'
        36     | 'XXXVI'
        47     | 'XLVII'
        89     | 'LXXXIX'
        99     | 'XCIX'
        890    | 'DCCCXC'
        1800   | 'MDCCC'
        1990   | 'MCMXC'
        2008   | 'MMVIII'
    }
}

